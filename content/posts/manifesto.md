---
title: "Manifesto"
date: 2020-02-05T18:20:26+01:00
draft: false
---
Human activity over the last decades has been changing our planet's climate by releasing greenhouse gases into the atmosphere. The Earth's atmosphere now contains about 410 ppm carbon dioxide (compared to 280 ppm in the 18th century) [1,2]. The resulting global warming is anticipated to cause considerable harm to human civilizations throughout the 21st century and beyond, particularly if the concentration rises above 450 ppm  [3]. To mitigate this, we must urgently reduce our carbon emissions.  There are initiatives on many levels of international politics setting goals for this reduction [4,5]. For instance, the IPCC panel of the United Nations advocates a 45% reduction of carbon dioxide emissions by 2030 relative to 2010 levels, to limit global warming to 1.5°C. [6].

As researchers in Theoretical Computer Science, we can , inprinciple, help address this challenge via our research.
However, we acknowledge that our activities also contribute to the problem, in particular by the greenhouse gas emissions caused by our travel.

We believe that our research community should aim for a significant reduction of carbon emissions.  We accept our responsibility to make our conference evolve towards a more sustainable model and therefore commit ourselves to the goal of reducing the carbon emissions of ... (our conference) by 50% until 2030.

We will explore several possible ways to achieve this goal, for instance by keeping track of our carbon footprint, encouraging sustainable travel, experimenting with remote participation, investigating possible options for location and scheduling, considering possible offsetting mechanisms, and discussing this issue and our progress at the conference.
[ + another option:  "making local arrangements such as social events, meals, and logistics as climate friendly as possible" ]
We will do so while striving to preserve the scientific quality and conviviality of the event, and its fairness with respect to all potential contributors and participants.


[1] en.wikipedia.org/wiki/Carbon_dioxide_in_Earth%27s_atmosphere

[2] www.co2.earth

[3] www.ipcc.ch/pdf/assessment-report/ar5/syr/AR5_SYR_FINAL_SPM.pdf

[4] www.un.org/management/news/un-secretariat-adopts-climate-action-plan

